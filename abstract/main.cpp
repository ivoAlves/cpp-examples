#include <iostream>

class Container{
public:
	Container(const Container&) = delete; 			//delete default copy operations
	Container& operator =(const Container&) = delete;

	Container(const Container&&) = delete;			//delete default move operations
	Container& operator=(Container&&) = delete;

	Container(int& a){_justAnInt = a;}
	virtual ~Container(){};
	void play(){
		//Container::doSmth(); this is not possible
		Container::doSmthWithInt();
		doSmthWithInt();
	};
protected:
	virtual void doSmth() = 0;
	virtual void doSmthWithInt(){
		std::cout << "do smth with " << _justAnInt << '\n';
	};

private:
	int _justAnInt;
};

class SuperContainer : public Container{

public:
	// ***** this is already deleted in Container class *****
	// SuperContainer(const SuperContainer&) = delete; 			//delete default copy operations
	// SuperContainer& operator =(const SuperContainer&) = delete;
	//
	// SuperContainer(const SuperContainer&&) = delete;			//delete default move operations
	// SuperContainer& operator=(SuperContainer&&) = delete;

	SuperContainer(int& value) : Container(value){};
	~SuperContainer(){};
	void play(){
		//play(); this is not possible
		Container::play();
		doSmth();
	};
protected:
	void doSmthWithInt(){
		std::cout << "i dont have access to _justAnInt" << '\n';
	};
	void doSmth(){
		std::cout << "doing something..." << '\n';
		doSmthWithInt();
	}
};

int main() {

	int a(3);
	//SuperContainer s = SuperContainer(a); this is not possible, default copy assignment deleted
	SuperContainer s(a);
	s.play();


	std::cout<<"end"<<std::endl;

}
