#include <iostream>
#include <vector>
#include <list>

template<typename T>
class LessThan
{
private:
	const T value;
public:
	LessThan(const T& v):value(v){}
	bool operator()(const T& x) const { return x<value;} //call operator
};

template<typename C, typename P>
int count(const C& c, P pred){
	int count =0;
	for(const auto& x : c)
		if(pred(x))
			++count;
	return count;
}

int main(int argc, char* argv[])
{
	std::vector<int> vi {1,2,3,4,5,6};
	std::list<double> ld {1.1, 1.2, 1.3, 1.4};

	//********** using functor (function object)

	std::cout << count(vi, LessThan<int>{5}) << "\n";
	std::cout << count(ld, LessThan<double>{1.25}) << "\n";

	//********** same but using lambda functor

	std::cout << count(vi, [&](int a){ return a<5; }) << "\n";
	std::cout << count(ld, [&](double a){ return a<1.25; }) << "\n";

	return 0;
}
