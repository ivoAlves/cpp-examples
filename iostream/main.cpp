#include <iostream>
#include <string>
#include <iterator>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>

struct Entry
{
	std::string name;
	int number;

};

std::ostream& operator<<(std::ostream& os, const Entry& e){
	return os << "{\"" << e.name << "\", " << e.number << "}";
}

std::istream& operator>>(std::istream& is, Entry& e){
	char c, c2;
	if(is>>c && c=='{' && is >> c2 && c2=='"'){ //if starts withi {"
		std::string name;
		while(is.get(c) && c != '"')
			name += c;

		if(is>>c && c==','){
			int number = 0;
			if(is>>number>>c && c=='}'){
				e = {name, number};
				return is;
			}
		}
	}
	is.setstate(std::ios_base::failbit); //failure in the stream
	return is;
}

int foo(){

	std::string from = "main.cpp";
	std::string to = "sortedMain.txt";

	std::ifstream is{from};
	std::ofstream os{to};

	//std::istream_iterator<std::string> ii {is};
	//std::istream_iterator<std::string> eos {};
	//std::ostream_iterator<std::string> oo {os, "\n"};

	// std::vector<std::string> b {ii, eos};
	// std::sort(b.begin(), b.end());
	// std::unique_copy(b.begin(), b.end(), oo);

	//or use a set to avoid sort and unique_copy later :)
	std::set<std::string> b {std::istream_iterator<std::string>{is}, std::istream_iterator<std::string>{}};
	copy(b.begin(), b.end(), std::ostream_iterator<std::string>{os, "\n"});

	return !is.eof() || !os;
}

int main(int argc, char* argv[])
{
	Entry e;
	for(Entry ee; std::cin>>e;)
		std::cout << e << '\n';


	//************

	std::ostream_iterator<std::string> oi {std::cout};

	*oi = "Hello, ";
	++oi;
	*oi = "wordl!\n";

	//*****************
	return foo();
}
