#include <iostream>

class Vector{
public:

	~Vector(){
		delete[] _elememts;
		std::cout << "destructor called" << "\n";
	}

	Vector(int s){
		_size = s;
		_elememts = new double[s];
		for (int i = 0; i < s; ++i)
		{
			_elememts[i] = 0;
		}
	}
	//copy-constructor
	Vector(const Vector& v):_elememts{new double[v.size()]}, _size{v.size()}{
		for (int i = 0; i < v.size(); ++i){
			_elememts[i] = v[i];
		}
	}
	//copy-assignment
	Vector& operator=(const Vector& v){

		double* a = new double[v.size()];
		for (int i = 0; i < v.size(); ++i){
			a[i] = v[i];
		}
		delete[] _elememts;
		_elememts = a;
		_size = v.size();
		return *this;
	}

	//move-constructor
	Vector(Vector&& v):_elememts{v._elememts}, _size{v._size}{
		v._elememts = nullptr;
		v._size = 0;
	}
	//move-assignment
	Vector& operator=(Vector&& v){
		_elememts = v._elememts;
		_size = v._size;
		v._elememts = nullptr;
		v._size = 0;
		return *this;
	}

	double& operator[](const int& i){
		return _elememts[i];
	}

	const double& operator[](const int& i) const{
		return _elememts[i];
	}

	int size() const{
		return _size;
	}

private:
	double* _elememts;
	int _size;

};

int main() {
	auto a = Vector(5);
	auto b = Vector(5);

	a[2] = 5;
	b[0] = a[2];
	a[2] = 3;

	for (int i = 0; i < a.size(); ++i){
		std::cout << "a " << a[i] << "\n";
	}
	for (int i = 0; i < b.size(); ++i){
		std::cout << "b " << b[i] << "\n";
	}

	auto grab {std::move(a)};

	std::cout << "grab size: " << grab.size() << "\n";
	std::cout << "a size: " << a.size() << "\n";

	grab [0] = 2;
	grab [1] = 2;
	grab [2] = 2;

	for (int i = 0; i < a.size(); ++i){
		std::cout << "a " << a[i] << "\n";
	}
	for (int i = 0; i < grab.size(); ++i){
		std::cout << "grab " << grab[i] << "\n";
	}

	b = std::move(grab); // b = grab;

	for (int i = 0; i < b.size(); ++i){
		std::cout << "b " << b[i] << "\n";
	}
	for (int i = 0; i < grab.size(); ++i){
		std::cout << "grab " << grab[i] << "\n";
	}


	std::cout<<"end"<<std::endl;

}
