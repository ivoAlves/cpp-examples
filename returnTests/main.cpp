#include <iostream>
#include <memory>

struct someStruct
{
	someStruct():member{0}{};
	int member;
};

someStruct returnCopy(){
	auto a = someStruct();
	a.member = 1;
	return a;
}

someStruct* returnRawPointer(){
	someStruct* a = new someStruct();
	a->member = 2;
	return a;
}

std::unique_ptr<someStruct> returnUniquePointer(){
	auto a = std::unique_ptr<someStruct>(new someStruct());
	a->member = 3;
	return a;
}

int main(int argc, char* argv[])
{
	auto a = returnCopy();
	std::cout << a.member << "\n";

	auto b = returnRawPointer();
	std::cout << b->member << "\n";
	delete b;

	auto c = returnUniquePointer();
	std::cout << c->member << "\n";

	return 0;
}
