
#include <iostream>
#include <memory>
#include <vector>

class A{
  public:
    A(){std::cout<<"default ctor"<<std::endl;};
    A(const int& value){ someValue = value; std::cout<<"copy ctor"<<std::endl;};
    ~A(){std::cout<<"destructor"<<std::endl;};
    int value(){return someValue;};
  private:
    int someValue = 3;
};

void foo(std::vector<std::shared_ptr<A>> &v)
{
  auto a = new A(1);

  auto ptr1 = std::shared_ptr<A>(a);
  auto ptr2 = std::shared_ptr<A>(a);

  v.push_back(ptr1);
  v.push_back(ptr2);

  std::cout<<"vector " << v.size() <<std::endl;
}

int main() {

  std::vector<std::shared_ptr<A>> v;
  foo(v);
  std::cout<<"vector size:" << v.size() <<std::endl;

  std::cout<<"v1 " << v.at(0)->value() <<std::endl;
  std::cout<<"v2 " << v.at(1)->value() <<std::endl;

}
