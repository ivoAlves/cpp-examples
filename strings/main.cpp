#include <iostream>
#include <string>


int main(int argc, char* argv[])
{
	std::string s = "Some string";

	std::string a = s.substr(0,4);

	std::string b = std::move(s.substr(0,4));
	std::cout << s << "\n";
	std::string c = std::move(s);

	std::cout << s << "\n";
	std::cout << a << "\n";
	std::cout << b << "\n";
	std::cout << c << "\n";

	return 0;
}
