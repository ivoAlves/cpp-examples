#include <iostream>
#include <vector>
#include <list>

template<typename T>
class Example
{
private:
	T _x,_y;

public:
	Example(const T& x, const T& y):_x{x}, _y{y}{
	};
	~Example() = default;

	T& x();
	T& y();
	T sum();

};

template<typename T>
T& Example<T>::x(){
	return _x;
}

template<typename T>
T& Example<T>::y(){
	return _y;
}

template<typename T>
T Example<T>::sum(){
	return _x+_y;
}

//function template specialization (also valid for class templates)
template<>
char Example<char>::sum(){
	return 's';
}

//**************

template<typename Container, typename Value>
Value foo(const Container& c, Value v){
 	for(auto x : c)
		v+=x;
	return v;
}

//**************** variadic template

void f(){}
template<typename T>
void g(T x){ std::cout << x << ' ';}
template<typename T, typename... Tail>
void f(T head, Tail... tail){
	g(head);
	f(tail...);
}

int main(int argc, char* argv[])
{
	auto a = Example<int>(3, 5);
	auto b = Example<float>(3.21,5.32);
	auto c = Example<char>('2','3');

	std::cout << a.sum() << "\n";
	std::cout << b.sum() << "\n";
	std::cout << c.sum() << "\n";

	//**************
	std::cout << "\n";

	std::vector<int> vi {1,2,3,4,5,6};
	std::list<double> ld {1.1, 1.1, 1.1, 1.1};

	std::cout << foo(vi, 100) << "\n";
	std::cout << foo(ld, 10.0) << "\n";

	//**************
	std::cout << "\n";
	
	f(0.2, 1, 3, "wth", ", this is awesome!", 3,2,1,"\n");

	return 0;
}
