#include <vector>
#include <iostream>
#include <functional>
#include <thread>

void f(const std::vector<double>& v, double* res){
	for(auto e : v)
		*res += e;
}

class F
{
private:
	const std::vector<double>& v;
	double* res;

public:
	F(const std::vector<double>& vv, double* p):v{vv}, res{p}{}
	void operator()(){
		for(auto e : v)
			*res += e;
	}
};

int main(int argc, char* argv[])
{
	std::vector<double> v1 {1,2,3.3,4.4,5.5,6.542};
	std::vector<double> v2 {4.4111,5.533,6.521};

	double r1;
	double r2;

	std::thread t1 {f,std::cref(v1), &r1};
	std::thread t2 {F{v2, &r2}};

	t1.join();
	t2.join();

	std::cout << r1 << "\n" << r2 << '\n';

	return 0;
}
