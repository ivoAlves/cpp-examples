#include <iostream>

template<typename T>
class Vector{
public:

	~Vector(){
		delete[] _elememts;
		std::cout << "destructor called" << "\n";
	}

	Vector(int s){
		_size = s;
		_elememts = new T[s];
		for (int i = 0; i < s; ++i)
		{
			_elememts[i] = 0;
		}
	}
	//copy-constructor
	Vector(const Vector& v):_elememts{new T[v.size()]}, _size{v.size()}{
		for (int i = 0; i < v.size(); ++i){
			_elememts[i] = v[i];
		}
	}
	//copy-assignment
	Vector& operator=(const Vector& v){

		T* a = new T[v.size()];
		for (int i = 0; i < v.size(); ++i){
			a[i] = v[i];
		}
		delete[] _elememts;
		_elememts = a;
		_size = v.size();
		return *this;
	}

	//move-constructor
	Vector(Vector&& v):_elememts{v._elememts}, _size{v._size}{
		v._elememts = nullptr;
		v._size = 0;
	}
	//move-assignment
	Vector& operator=(Vector&& v){
		_elememts = v._elememts;
		_size = v._size;
		v._elememts = nullptr;
		v._size = 0;
		return *this;
	}

	T& operator[](const int& i){
		return _elememts[i];
	}

	const T& operator[](const int& i) const{
		return _elememts[i];
	}

	int size() const{
		return _size;
	}

private:
	T* _elememts;
	int _size;

};

int main() {
	auto a = Vector<int>(5);

	a[3] = 3;

	std::cout<<"end"<<std::endl;

}
